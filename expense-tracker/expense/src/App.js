import "./App.css";
import Expense from "./components/Expense";

function App() {
  return (
    <div className="App">
      <header>
        <h2>Expense Tracker</h2>
      </header>
      <Expense />
    </div>
  );
}

export default App;
