import { useEffect, useState } from "react";
import OverView from "./OverView";
import TransAction from "./TransaAction";

const Expense = () => {
  const [expense, setExpense] = useState(0);

  const [income, setIncome] = useState(0);

  const [transaction, setTransaction] = useState([]);

  const addTrasaction = (formValue) => {
    console.log(formValue);
    setTransaction([...transaction, { ...formValue, id: Date.now() }]);
  };

  useEffect(() => {
    let inc = 0;
    let exp = 0;
    transaction.forEach((t) => {
      t.type === "expense"
        ? (exp = exp + parseFloat(t.amount))
        : (inc = inc + parseFloat(t.amount));
    });
    setExpense(exp);
    setIncome(inc);
  }, [transaction]);

  return (
    <section className="section">
      <OverView
        income={income}
        expense={expense}
        addTrasaction={addTrasaction}
      />

      <TransAction transaction={transaction} />
    </section>
  );
};

export default Expense;
