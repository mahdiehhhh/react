import { useState } from "react";

const TransaActionForm = ({ addTrasaction, setisShow }) => {
  const [formValue, setFomrValues] = useState({
    type: "expense",
    desc: "",
    amount: 0,
  });
  const changeHandler = (e) => {
    setFomrValues({ ...formValue, [e.target.name]: e.target.value });
  };

  const submitHandler = (e) => {
    e.preventDefault();
    addTrasaction(formValue);
    setisShow(false);
  };

  return (
    <form onSubmit={submitHandler}>
      <div className="input__div">
        <input
          type="text"
          name="desc"
          onChange={changeHandler}
          value={formValue.desc}
        />

        <input
          type="number"
          name="amount"
          onChange={changeHandler}
          value={formValue.amount}
        />
      </div>

      <div className="radio__div">
        <input
          type="radio"
          value="expense"
          name="type"
          checked={formValue.type === "expense"}
          id="expense"
          onChange={changeHandler}
        />

        <label htmlFor="expense">Expense</label>

        <input
          type="radio"
          value="income"
          name="type"
          checked={formValue.type === "income"}
          onChange={changeHandler}
          id="income"
        />

        <label htmlFor="income">Income</label>
      </div>

      <button
        type="submit"
        className="balance__style-button"
        style={{ margin: "10px 0 10px 0", width: "100%" }}
      >
        Add Transaction
      </button>
    </form>
  );
};

export default TransaActionForm;
