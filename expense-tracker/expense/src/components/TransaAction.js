import { useEffect, useState } from "react";

const TransAction = ({ transaction }) => {
  const [searchItem, setSearchItem] = useState("");

  const [filterTransaction, setfilterTransaction] = useState(transaction);

  const filteredTransaction = (search) => {
    if (!search) {
      setfilterTransaction(transaction);
      return;
    }
    const filtered = transaction.filter((t) =>
      t.desc.toLowerCase().includes(search)
    );
    setfilterTransaction(filtered);
  };

  const changeHandler = (e) => {
    setSearchItem(e.target.value);
    filteredTransaction(e.target.value);
  };

  useEffect(() => {
    filteredTransaction(searchItem);
  }, [transaction]);

  return (
    <div>
      <input type="text" value={searchItem} onChange={changeHandler} />
      {filterTransaction.map((t) => {
        return (
          <div key={t.id} className="transaction">
            <span>description: {t.desc}</span>

            <span>Amount: {t.amount}</span>
          </div>
        );
      })}
    </div>
  );
};

export default TransAction;
