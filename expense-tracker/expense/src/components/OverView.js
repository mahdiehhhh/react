import { useState } from "react";
import TransaActionForm from "./TransActionForm";
const OverView = ({ income, expense, addTrasaction }) => {
  const [isShow, setisShow] = useState(false);
  return (
    <>
      <div className="balance__style">
        <span className="balance__style-text">Balance: {income - expense}</span>
        <button
          className="balance__style-button"
          onClick={() => setisShow(!isShow)}
        >
          {isShow ? " Cancle" : "Add"}
        </button>
      </div>
      {isShow && (
        <TransaActionForm addTrasaction={addTrasaction} setisShow={setisShow} />
      )}
      <div className="balance">
        <div className="balance__div">
          <span>expense</span>

          <span style={{ color: "red" }}>{expense}$</span>
        </div>

        <div className="balance__div">
          <span>income</span>

          <span style={{ color: "green" }}>{income}$</span>
        </div>
      </div>
    </>
  );
};

export default OverView;
