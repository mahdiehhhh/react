import "../App.css";

const Todo = ({ todo, onCompelete, onRemove,onEdit }) => {
  return (
    <div key={todo.id} className="todoStyle">
      <p className={todo.isCompleted ? "compeleted" : "todoStyle__text"}>
        {todo.text}
      </p>

      <div>
        <button className="todoStyle__edit" onClick={onEdit}>Edit</button>
        
        <button className="todoStyle__compeleted" onClick={onCompelete}>
          compelete
        </button>

        <button className="todoStyle__remove" onClick={onRemove}>
          remove
        </button>
      </div>
    </div>
  );
};

export default Todo;
