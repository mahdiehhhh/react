import React, { useEffect, useRef, useState } from "react";
import "../App.css";

const TodoForm = (props) => {
  const [input, setInput] = useState(props.edit ? props.edit.text : "");

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  });

  const todoHandler = (e) => {
    setInput(e.target.value);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (!input) {
      alert("input is empty");
      return;
    } else {
      props.submitTodo(input);
      setInput("");
    }
  };

  return (
    <form onSubmit={submitHandler}>
      <>
        <input
          type="text"
          value={input}
          onChange={todoHandler}
          className={
            props.edit ? "todoStyle__input-update" : "todoStyle__input"
          }
          placeholder={props.edit ? "update value" : "Add Todo"}
          ref={inputRef}
        />

        <button type="submit" className="todoStyle__addbutton">
          {props.edit ? "Update" : "Add"}
        </button>
      </>
    </form>
  );
};

export default TodoForm;
