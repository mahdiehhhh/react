import "../App.css";
import React from "react";
import Select  from "react-select";

const options = [
  { value: "All", label: "All" },
  { value: "Uncompeleted", label: "Uncompeleted" },
  { value: "compeleted", label: "compeleted" },
];

const TodoNav = ({ uncompeleted, onSelected, status }) => {
  if (!uncompeleted)
    return <h2 className="uncompelete__title">set your Today Todo</h2>;

  return (
    <div className="navstyle">
      <div className="navstyle__div">
        <span>unCompeleted Todo :</span>
        <p className="uncompelete">{uncompeleted}</p>
      </div>

      <Select  value={status} onChange={onSelected} options={options} />
    </div>
  );
};

export default TodoNav;
