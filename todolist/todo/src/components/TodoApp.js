import { useEffect, useState } from "react";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";
import TodoNav from "./TodoNav";

const Todos = () => {
  const [todos, setTodos] = useState([]);
  const [filteredStatus, setfilteredStatus] = useState([]);
  const [status, setStatus] = useState("All");

  useEffect(() => {
    filterTodo(status);
  }, [todos, status]);

  const submitTodo = (input) => {
    const newTodo = {
      id: Math.floor(Math.random() * 1000),
      text: input,
      isCompleted: false,
    };
    setTodos([...todos, newTodo]);
  };

  const compeleteTodo = (id) => {
    const TodoId = todos.findIndex((todo) => todo.id === id);
    const selesctedTodo = { ...todos[TodoId] };
    selesctedTodo.isCompleted = !selesctedTodo.isCompleted;
    const updatedTodos = [...todos];
    updatedTodos[TodoId] = selesctedTodo;
    setTodos(updatedTodos);
  };

  const removeTodo = (id) => {
    const filterTodo = todos.filter((todo) => todo.id !== id);
    setTodos(filterTodo);
  };

  const updateTodo = (id, newValue) => {
    const TodoId = todos.findIndex((todo) => todo.id === id);
    const selesctedTodo = { ...todos[TodoId] };
    selesctedTodo.text = newValue;
    const updatedTodos = [...todos];
    updatedTodos[TodoId] = selesctedTodo;
    setTodos(updatedTodos);
  };

  const filterTodo = (status) => {
    switch (status) {
      case "All":
        setfilteredStatus(todos);
        break;
      case "compeleted":
        setfilteredStatus(todos.filter((t) => t.isCompleted));
        break;
      case "Uncompeleted":
        setfilteredStatus(todos.filter((t) => !t.isCompleted));
        break;
      default:
    }
  };

  const selectChange = (e) => {
    filterTodo(e.value);
    setStatus(e.value);
  };
  return (
    <>
      <TodoNav
        uncompeleted={todos.filter((t) => !t.isCompleted).length}
        status={status}
        onSelected={selectChange}
      />
      <div className="container">
        <TodoForm submitTodo={submitTodo} />
        <TodoList
          todos={filteredStatus}
          onCompelete={compeleteTodo}
          onRemove={removeTodo}
          onUpdateTodo={updateTodo}
        />
      </div>
    </>
  );
};

export default Todos;
