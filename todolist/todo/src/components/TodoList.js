import React, { useState } from "react";
import Todo from "./Todo";
import TodoForm from "./TodoForm";

const TodoList = ({ todos, onCompelete, onRemove, onUpdateTodo }) => {
  const [edit, setEdit] = useState({
    id: null,
    text: "",
    isCompleted: false,
  });

  const editTodoHndler = (newValue) => {
    onUpdateTodo(edit.id, newValue);
    setEdit({
      id: null,
      text: "",
      isCompleted: false,
    });
  };
  const RenderTodo = () => {
    if (todos.length === 0) return <p className="todoStyle__text">add Todo</p>;
    return todos.map((todo) => {
      return (
        <Todo
          key={todo.id}
          todo={todo}
          onCompelete={() => onCompelete(todo.id)}
          onRemove={() => onRemove(todo.id)}
          onEdit={() => setEdit(todo)}
        />
      );
    });
  };
  return (
    <div>
      {edit.id ? (
        <TodoForm submitTodo={editTodoHndler} edit={edit} />
      ) : (
        RenderTodo()
      )}
    </div>
  );
};

export default TodoList;
