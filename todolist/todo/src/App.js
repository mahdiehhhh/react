
import "./App.css";
import TodoForm from "./components/TodoApp";
function App() {
  return (
    <div className="App">
      <TodoForm />
    </div>
  );
}

export default App;
