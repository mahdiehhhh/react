import { useState } from "react";
import { useProductAction } from "../components/provider/ProductsProvider";
import styleCss from "./search.module.css";
const Search = ({ filter }) => {
  const dispatch = useProductAction();
  const [value, setValue] = useState("");
  const searchHandler = (e) => {
    dispatch({ type: "filter", selectedOption: filter });
    dispatch({ type: "search", event: e });
    setValue(e.target.value);
  };
  return (
    <div className={styleCss.search}>
      <span>Search For :</span>
      <input
        value={value}
        onChange={searchHandler}
        className={styleCss.search__input}
        type="text"
        placeholder="Search For ...."
      />
    </div>
  );
};

export default Search;
