import React from "react";
import styleProduct from "./Product.module.css";
import { BiTrash } from "react-icons/bi";

const Product = ({ product, onInput, onIncrement, onDcrement, onDelete }) => {
  return (
    <div className={styleProduct.product}>
      <p>product name :{product.title} </p>

      <p>product price : {product.price}</p>

      {/* <input type='text' onChange={onInput} value={product.name}/> */}

      <span className={styleProduct.value}>{product.quantity}</span>

      <button
        onClick={onIncrement}
        className={`${styleProduct.Btn} ${styleProduct.inc}`}
      >
        +
      </button>

      <button
        onClick={onDcrement}
        className={`${product.quantity === 1 && styleProduct.remove} ${
          styleProduct.inc
        }`}
      >
        {product.quantity > 1 ? (
          "-"
        ) : (
          <span>
            <BiTrash />
          </span>
        )}
      </button>

      <button onClick={onDelete} className={styleProduct.Btn}>
        delete
      </button>
    </div>
  );
};

export default Product;
