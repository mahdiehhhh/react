import { useProduct } from "../provider/ProductsProvider";
import styleProduct from "./Product.module.css";
const ProductNav = ({ product }) => {
  const products = useProduct();
  const total = products.filter((p) => p.quantity > 0).length;
  return (
    <div className={styleProduct.title}>
      <p>product = {product}</p>
      <span>{total}</span>
    </div>
  );
};

export default ProductNav;
