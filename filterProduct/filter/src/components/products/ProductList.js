import Product from "./Products";
import styleProduct from "./Product.module.css";
import { useProduct, useProductAction } from "../provider/ProductsProvider";

const ProductList = () => {
  const products = useProduct();
  const dispatch = useProductAction();
  if (products.length === 0) {
    return (
      <div className={styleProduct.empty__basket}>
        there is no product in basket
      </div>
    );
  }
  return (
    <div className={styleProduct.main__product}>
      {products.map((product, index) => {
        return (
          <Product
            product={product}
            key={index}
            onDelete={() => dispatch({ type: "remove", id: product.id })}
            onIncrement={() => dispatch({ type: "increment", id: product.id })}
            onDcrement={() => dispatch({ type: "decrement", id: product.id })}
            onInput={(e) =>
              dispatch({ type: "change", id: product.id, event: e })
            }
          />
        );
      })}
    </div>
  );
};

export default ProductList;
