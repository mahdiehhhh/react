import React, { useContext, useReducer } from "react";

import { productsData } from "../../db/products";

import _ from "lodash";

const productsContext = React.createContext(); //state

const productsContextDispatcher = React.createContext(); //action

const reducer = (state, action) => {
  switch (action.type) {
    case "remove": {
      const filterProduct = state.filter((p) => p.id !== action.id);
      return filterProduct;
    }

    case "increment": {
      const index = state.findIndex((item) => item.id === action.id);
      const product = { ...state[index] };
      product.quantity++;
      const updateProducts = [...state];
      updateProducts[index] = product;
      return updateProducts;
    }

    case "decrement": {
      const index = state.findIndex((item) => item.id === action.id);
      const product = { ...state[index] };

      if (product.quantity === 1) {
        const filterProduct = state.filter((p) => p.id !== action.id);
        return filterProduct;
      } else {
        product.quantity--;
        const updateProducts = [...state];
        updateProducts[index] = product;
        return updateProducts;
      }
    }
    case "filter": {
      if (action.selectedOption.value === "") {
        return productsData;
      } else {
        const updatedProduct = productsData.filter(
          (p) => p.availableSizes.indexOf(action.selectedOption.value) >= 0
        );
        return updatedProduct;
      }
    }
    case "sortFilter": {
      const value = action.selectedOption.value;
      if (value === "highest") {
        return _.orderBy(state, ["price"], ["desc"]);
      } else {
        return _.orderBy(state, ["price"], ["asc"]);
      }
    }
    case "search": {
      const value = action.event.target.value;
      if (value === "") {
        return state;
      } else {
        const filteredProducts = state.filter((p) =>
          p.title.toLowerCase().includes(value.toLowerCase())
        );
        return filteredProducts;
      }
    }
    default:
      return state;
  }
};

const ProductsProvider = ({ children }) => {
  const [products, dispatch] = useReducer(reducer, productsData);

  return (
    <productsContext.Provider value={products}>
      <productsContextDispatcher.Provider value={dispatch}>
        {children}
      </productsContextDispatcher.Provider>
    </productsContext.Provider>
  );
};

export default ProductsProvider;

export const useProduct = () => useContext( );

// export const useProductAction = () => useContext(productsContextDispatcher);

export const useProductAction = () => {
  return useContext(productsContextDispatcher);
};
