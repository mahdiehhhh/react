import { useState } from "react";
import { useProductAction } from "../provider/ProductsProvider";
import styleFilter from "./filter.module.css";
import Select from "react-select";
import Search from "../../common/Search";

const FilterProduct = () => {
  const dispatch = useProductAction();

  const [filter, setFilter] = useState("");

  const [sort, setSort] = useState("");

  const filterOptions = [
    { value: "", label: "All" },
    { value: "XXL", label: "XXL" },
    { value: "XL", label: "XL" },
    { value: "L", label: "L" },
    { value: "M", label: "M" },
    { value: "X", label: "X" },
    { value: "S", label: "S" },
  ];
  const sortOptions = [
    { value: "highest", label: "highest" },
    { value: "lowest", label: "lowest" },
  ];

  const filterHandler = (selectedOption) => {
    dispatch({ type: "filter", selectedOption });
    dispatch({ type: "sortFilter", selectedOption: sort });
    setFilter(selectedOption);
  };

  const sortHandler = (selectedOption) => {
    dispatch({ type: "sortFilter", selectedOption });
    setSort(selectedOption);
  };

  return (
    <div>
      <Search filter={filter} />
      <div className={styleFilter.filter}>
        <p>filter products based on :</p>

        <div className={styleFilter.filter__selectbox}>
          <p>order by: </p>

          <Select
            value={filter}
            onChange={filterHandler}
            options={filterOptions}
          />
        </div>

        <div className={styleFilter.filter__selectbox}>
          <p>Sort by: </p>

          <Select value={sort} onChange={sortHandler} options={sortOptions} />
        </div>
      </div>
    </div>
  );
};

export default FilterProduct;
