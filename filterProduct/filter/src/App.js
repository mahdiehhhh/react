
import FilterProduct from "./components/Filter/FilterProduct";
import ProductList from "./components/products/ProductList";
import ProductNav from "./components/products/ProductNav";
import ProductsProvider from "./components/provider/ProductsProvider";


const App = () => {
  return (
    <>
      <ProductsProvider>
        <div className="title_app" id="title">
          <ProductNav />
          
          <FilterProduct/>
          <ProductList />
        </div>
      </ProductsProvider>
    </>
  );
};

export default App;
