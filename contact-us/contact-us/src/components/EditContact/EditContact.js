import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import getOneContact from "../../Services/getOneContact";
import updateContact from "../../Services/updateContact";

const EditContact = () => {
  const [contact, setContact] = useState({
    name: "",
    email: "",
  });

  const { id } = useParams();

  const history = useNavigate();

  useEffect(() => {
    const localFetch = async () => {
      try {
        const { data } = await getOneContact(id);
        setContact({ name: data.name, email: data.email });
      } catch (error) {}
    };
    localFetch();
  }, [id]);

  const inputHandler = (e) => {
    setContact({ ...contact, [e.target.name]: e.target.value });
  };

  const submitHandler = async (e) => {
    if (contact.name && contact.email) {
      e.preventDefault();
      try {
        await updateContact(id, contact);
        history("/");
      } catch (error) {}
    } else {
      alert("all fildes are mandatory !");
    }
    setContact({
      name: "",
      email: "",
    });
  };

  return (
    <form onSubmit={submitHandler} className="form__style">
      <label htmlFor="name"> name:</label>

      <input
        type="text"
        id="name"
        name="name"
        className="input__style"
        onChange={inputHandler}
        value={contact.name}
      ></input>

      <label htmlFor="email">email:</label>

      <input
        type="email"
        id="email"
        name="email"
        className="input__style"
        value={contact.email}
        onChange={inputHandler}
      ></input>

      <button className="add__btn" type="submit">
        Update Contact
      </button>
    </form>
  );
};

export default EditContact;
