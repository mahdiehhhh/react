import { useState } from "react";
import "./addconatct.css";
import addContacts from "../../Services/addContactServices";
import { useNavigate } from "react-router-dom";
const AddConatct = () => {
  const [contact, setContact] = useState({
    name: "",
    email: "",
  });

  const history = useNavigate();

  const inputHandler = (e) => {
    setContact({ ...contact, [e.target.name]: e.target.value });
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    if (contact.name && contact.email) {
      try {
        await addContacts(contact);
        setContact({
          name: "",
          email: "",
        });
        history("/");
      } catch (error) {}
    } else {
      alert("all fileds are mandatory");
    }
  };

  return (
    <form onSubmit={submitHandler} className="form__style">
      <label htmlFor="name"> name:</label>

      <input
        type="text"
        id="name"
        name="name"
        className="input__style"
        onChange={inputHandler}
        value={contact.name}
      ></input>

      <label htmlFor="email">email:</label>

      <input
        type="email"
        id="email"
        name="email"
        className="input__style"
        value={contact.email}
        onChange={inputHandler}
      ></input>

      <button className="add__btn" type="submit">
        Add Contact
      </button>
    </form>
  );
};

export default AddConatct;
