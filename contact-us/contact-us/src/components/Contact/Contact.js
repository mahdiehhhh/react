import user from "../../image/user.webp";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import "./contact.css";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import getContacts from "../../Services/getContactService";
import deleteContacts from "../../Services/deleteContactService";

const Contact = () => {
  const [contacts, setContacts] = useState([]);

  const [allContacts, setAllContacts] = useState([]);

  const [searchTerm, setsearchTerm] = useState("");

  useEffect(() => {
    const fetchContacts = async () => {
      const { data } = await getContacts();

      setContacts(data);

      setAllContacts(data);
    };
    try {
      fetchContacts();
    } catch (error) {
      console.log(error);
    }
  }, []);

  const deleteContactHandler = async (id) => {
    try {
      await deleteContacts(id);

      let deleteId = contacts.filter((c) => c.id !== id);

      setContacts(deleteId);
    } catch (error) {
      console.log(error);
    }
  };

  const searchHandler = (e) => {
    const event = e.target.value;

    if (event !== " ") {
      setsearchTerm(event);
      const filterContacts = allContacts.filter((c) => {
        return Object.values(c)
          .join(" ")
          .toLowerCase()
          .includes(event.toLowerCase());
      });
      setContacts(filterContacts);
    } else {
      setContacts(allContacts);
    }
  };

  return (
    <>
      <Link to="/add" style={{ textDecoration: "none" }}>
        <button className="add__btn">Add Contact</button>
      </Link>

      <div>
        <input
          type="text"
          value={searchTerm}
          onChange={searchHandler}
          className="search__Style"
        />
      </div>

      {contacts ? (
        contacts.map((c, index) => {
          return (
            <section className="contact__style" key={index}>
              <div className="conatct__info">
                <img src={user} width="70px" alt="user" />

                <div>
                  <p className="conatct__info-text">name: {c.name}</p>

                  <p className="conatct__info-text">email : {c.email}</p>
                </div>
              </div>

              <div>
                <Link to={`/edit/${c.id}`}>
                  <button className="edit">Edit</button>
                </Link>
                <button
                  onClick={() => deleteContactHandler(c.id)}
                  className="trash"
                >
                  <FontAwesomeIcon icon={faTrash} />
                </button>
              </div>
            </section>
          );
        })
      ) : (
        <p>loading .........</p>
      )}
    </>
  );
};

export default Contact;
