import http from "./httpServices";

export default function deleteContacts(id) {
  return http.delete(`contacts/${id}`);
}
