import http from "./httpServices";

export default function addContacts(contact) {
  return http.post(`/contacts`, contact);
}
