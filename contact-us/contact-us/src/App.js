import "./App.css";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AddConatct from "./components/AddConatct/AddConatct";
import Contact from "./components/Contact/Contact";
import EditContact from "./components/EditContact/EditContact";
import { Route, Routes } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />

      <Routes>
        <Route path="/edit/:id" element={<EditContact />} />

        <Route path="/" exact element={<Contact />} />

        <Route path="/add" element={<AddConatct />} />
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
