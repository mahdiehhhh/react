import AddConatct from "./components/AddConatct/AddConatct";
import Contact from "./components/Contact/Contact";

const routes = [
  { path: "/", component: Contact, exact: true },
  { path: "add-new-contact", component: AddConatct },
];

export default routes;
