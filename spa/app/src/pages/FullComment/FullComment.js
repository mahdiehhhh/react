import { useEffect, useState } from "react";
import { useParams } from "react-router";
import http from "../../Service/HttpService";

const FullComment = ({ history }) => {
  const { id } = useParams();
  console.log("commentId111", id);
  console.log(history);
  const [selectComment, setselectComment] = useState(null);

  useEffect(() => {
    if (id) {
      http
        .get(`http://localhost:3001/comments/${id}`)
        .then((response) => {
          setselectComment(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [id]);

  const deleteHandler = async (id) => {
    try {
      await http.delete(`http://localhost:3001/comments/${id}`);
      history.push("/");
      setselectComment(null);
    } catch (error) {
      console.log("err", error);
    }
  };

  let commdentDetail = (
    <p style={{ margin: "0 0 20px 0", textAlign: "center" }}>
      please Select comment!
    </p>
  );

  if (id) commdentDetail = <p>loading.....</p>;

  if (selectComment) {
    commdentDetail = (
      <>
        <div className="full__comment-content">
          <p>name : {selectComment.name}</p>
          <p>Email:{selectComment.email}</p>
          <p>body:{selectComment.body}</p>
          <button className="delet__btn" onClick={() => deleteHandler(id)}>
            delete
          </button>
        </div>
      </>
    );
  }
  return commdentDetail;
};

export default FullComment;
