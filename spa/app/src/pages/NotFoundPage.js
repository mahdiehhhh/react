import image from "../image/404.jpg";
const NotFoundPage = () => {
  return (
    <>
      <img src={image} alt="404" width="100%" height="620px" />
    </>
  );
};

export default NotFoundPage;
