import { useState } from "react";
import http from "../../Service/HttpService";
import "../NewComment/newcomment.css";
const NewComment = ({ history }) => {
  const [comment, setComment] = useState({
    name: "",
    email: "",
    body: "",
  });

  const inputHandler = (e) => {
    setComment({ ...comment, [e.target.name]: e.target.value });
  };
  const postCommentHandler = async () => {
    try {
      await http.post("/comments", { ...comment, postId: 7 });
      history.push("/");
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="new__comment">
      <label htmlFor="name" className="new__comment-label">
        Name:
        <input
          id="name"
          type="text"
          className="new__comment-input"
          name="name"
          onChange={inputHandler}
        />
      </label>

      <label htmlFor="email" className="new__comment-label">
        Email:
        <input
          id="email"
          type="email"
          className="new__comment-input"
          name="email"
          onChange={inputHandler}
        />
      </label>

      <label htmlFor="comment" className="new__comment-label">
        Comment:
        <textarea
          id="comment"
          type="text"
          className="new__comment-input"
          name="body"
          onChange={inputHandler}
        ></textarea>
      </label>

      <button className="btn__add" onClick={postCommentHandler}>
        Add Comment
      </button>
    </div>
  );
};

export default NewComment;
