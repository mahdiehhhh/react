import "./comments.css";
const Comments = ({ name, email, commentIdHandler }) => {
  return (
    <div className="comment" onClick={commentIdHandler}>
      <span>name : {name}</span>
      <span>Email:{email}</span>
    </div>
  );
};

export default Comments;
