import { useEffect, useState } from "react";
import http from "../../Service/HttpService";
import "./allcomment.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Comments from "../Comments/Comments";
import FullComment from "../../pages/FullComment/FullComment";
import { Link } from "react-router-dom";
const AllComments = () => {
  const [showComment, setshowComment] = useState(null);

  const [er, setError] = useState(null);

  const [fullcomment, setfullcomment] = useState(null);

  useEffect(() => {
    http
      .get("http://localhost:3001/comments")
      .then((response) => {
        setshowComment(response.data);
      })
      .catch((error) => {
        setError(error.message);
      });
  }, []);

  const showToastMessage = () => {
    toast.error(er, {
      position: toast.POSITION.TOP_RIGHT,
    });
  };

  const renderComment = () => {
    let renderValue = <p>loading.....</p>;
    if (er) {
      renderValue = showToastMessage();
    }

    if (showComment && !er) {
      renderValue = showComment.map((c) => (
        <Link to={`/comments/${c.id}`} key={c.id}>
          <Comments
            name={c.name}
            email={c.email}
            commentIdHandler={() => commentIdHandler(c.id)}
          />
        </Link>
      ));
    }
    return renderValue;
  };

  const commentIdHandler = (id) => {
    setfullcomment(id);
  };

  return (
    <>
      <div className="show__comment">
        {renderComment()}

        <ToastContainer />
      </div>
      <div>
        <FullComment commentId={fullcomment} />
      </div>
    </>
  );
};

export default AllComments;
