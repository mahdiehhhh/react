import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import "./Header.css";
const items = [
  { name: "comments", to: "/", exact: true },
  { name: "NewComments", to: "/new-comment", exact: false },
];
const Navigation = () => {
  return (
    <header>
      <nav className="nav">
        <ul className="nav__items">
          {items.map((item) => (
            <li key={item.name}>
              <Link to={item.to} exact={item.exact || false}>
                {item.name}
              </Link>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  );
};

export default withRouter(Navigation);
