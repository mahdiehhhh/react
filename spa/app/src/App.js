import { BrowserRouter, Route, Switch } from "react-router-dom";
import routes from "./route";
import Layout from "./Layout/Layout";

const App = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          {routes.map((route) => (
            <Route {...route} />
          ))}
        </Switch>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
