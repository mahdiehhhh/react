import FullComment from "./pages/FullComment/FullComment";
import NewComment from "./pages/NewComment/NewComment";
import Home from "./pages/Home";
import NotFoundPage from "./pages/NotFoundPage";

const routes = [
  { path: "/comments/:id", component: FullComment },
  { path: "/new-comment", component: NewComment },
  { path: "/", component: Home, exact: true },

  { component: NotFoundPage },
];

export default routes;
